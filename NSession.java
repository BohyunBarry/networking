
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 *
 * @author Thu Le
 */
import java.io.*;
import java.net.*;
import java.text.DecimalFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NSession extends Thread {

    private Thread t;
    private String threadName;
    private InetAddress client;
    private int port;

    public NSession() {
    }

    public NSession(InetAddress client, int port) {
        this.threadName = "thisRandomDude" + Math.random();
        this.client = client;
        this.port = port;
    }

    public void run() {
        try {
            System.out.println("start: " + System.currentTimeMillis() );
            t.sleep(10000);
            System.out.println("start: " + System.currentTimeMillis());

        } catch (InterruptedException ex) {
            Logger.getLogger(NSession.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            DatagramSocket s = new DatagramSocket();

            String answerString = "TimeOut";
            byte[] sendData;

//            String capitalizedSentence = sentence.toUpperCase();
            String capitalizedSentence = answerString;
            sendData = capitalizedSentence.getBytes();

            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, client, port);
            s.send(sendPacket);
            System.out.println(this.threadName);
			s.close();

        } catch (Exception ex) {
//            Logger.getLogger(NSession.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
